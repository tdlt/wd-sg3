var fs = require('fs')

exports.write = function (msg) {
    var data = {date:  new Date().toLocaleString(), msg: msg};
    fs.appendFile('logs.json', JSON.stringify(data) + ',\n');
}

exports.flush = function () {
    fs.writeFileSync('logs.json', '');
}