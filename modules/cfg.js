var nconf = require('nconf');
nconf.add('config', {type: 'file', file: './cfg.json'})
nconf.load();

module.exports = nconf;