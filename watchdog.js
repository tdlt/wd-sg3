/**
 * Created by mushfau on 7/1/2017.
 */
var socketClusterClient = require('socketcluster-client'),
    cfg = require('./modules/cfg.js'),
    request = require('request'),
    Promise = require('promise'),
    checksum = require('checksum'),
    exec = require('child_process').exec,
    path = require('path'),
    fs = require('fs'),
    fse = require('fs-extra'),
    cli = require('cli'),
    os = require('os'),
    jsonfile = require('jsonfile');
    log = require('./modules/log_helper');   

var config;
var options;
var wd = {};
var server_reconnect_count = 0;
var player_heartbeat_time;
var new_downloads = false;
var status_promise = null;
var screenstatus_promise = null;
var downloading = false;
wd.serverSocketCluster = null;
wd.playerSocketListener = null;
wd.playerSocket = null;
wd.screenSocket = null;

var events = require('events');
var eventEmitter = new events.EventEmitter();

var clientChannel = null;


function init() {
    return new Promise(function (fulfill, reject) {

        if (cfg.get('config') === undefined) {
            var servers = cfg.get('servers');
            var backend = {};
            var cdn = {};
            servers.forEach(function (server) {
                if (server.type === "backend") {
                    backend = server;
                }
                else if (server.type === "cdn") {
                    cdn = server;
                }
            });

            config = {
                "server_ip": backend.ip,
                "server_port": backend.port,
                "cdn_server_ip": cdn.ip,
                "cdn_server_port": cdn.port,
                "screen_ip": "192.168.1.246",
                "screen_port": "1515",
                "version": "2.1",
                "port": 22222,
                "player": {
                    "player_app": "player.exe",
                    "player_path": "/data/player/",
                    "Process_name": "player",
                    "app_folder": "player_v0/",
                    "chk_interval": 360000
                },
                "media": {
                    "media_path": "/data/medias/"
                },
                "pid": "",
                "player_name": cfg.get('player_name'),
                "cid": "",
                "cpn_date": "",
                "downloadedAt": "",
                "log_path": "./data/logs/log.xml",
                "client_key": cfg.get('license_key'),
                "debugging": false,
                "connect_screen": false,
                "restart_player_on_failure": true,
                "restart_limit": 30,
                "server_reconnect_on_validation_failure": true,
                "server_reconnect_limit": 3,
                "player_heartbeat_interval": 5,
                "email": "support@tractive.com.my"
            }

            cfg.set('config', config);
            cfg.save();

            options = {
                hostname: config.server_ip,
                port: config.server_port,
                secure: false,
                ackTimeout: 5000,
                autoReconnect: true,
                autoConnect: true
            };

        } else {
            config = cfg.get('config');
            options = {
                hostname: config.server_ip,
                port: config.server_port,
                secure: false,
                ackTimeout: 5000,
                autoReconnect: true,
                autoConnect: true
            };
        }
        cli.info('Starting watchdog version ' + config.version + '\n')
        fulfill();
    });


}

init().then(function () {
    runPlayer()
    connectToPlayer();
    setTimeout(function () {
        connectToServer();
    }, 3000);
})

function runPlayer() {
    if (os.type() != "Linux") {
        var playerPath = path.join(__dirname + config.player.player_path, config.player.app_folder);
        exec('player.exe', {cwd: playerPath}
            , function (error, stdout, stderr) {
            }
        ).on('close', function (code) {
            return code;
        });
    }
}

function connectToServer() {
    cli.info('WD - connecting to server');
    wd.serverSocketCluster = socketClusterClient.connect(options);

    wd.serverSocketCluster.on('connect', function (socket) {

        cli.ok("WD - socket connected: " + socket.id);

        if (wd.serverSocketCluster.state == 'open') {
            process.update_ready = true;

            if (config.pid == "") {
                registerNode();
            } else {
                connectNode();
            }

            clientChannel = wd.serverSocketCluster.subscribe(config.client_key);
            cli.info("WD - subscription on connect " + wd.serverSocketCluster.subscriptions(true));

            clientChannel.watch(function (data) {

                if (typeof data.players == "undefined") {

                    switch (data.action) {
                        case 'campaign:launch' :
                            if (config.cid == data.campaign.cpn_cd) {

                                cli.ok("Campaign Broadcasted: " + data.campaign.cpn_cd.toUpperCase());
                                launchCampaign(data.campaign, 'launch');
                            }
                            break;
                    }
                }
                else if (data.players.indexOf(config.pid) != -1) {
                    switch (data.action) {
                        case 'player:launch' :
                            cli.ok("WD - campaign launch received")
                            var campaign = data.campaign;
                            if (config.cid !== campaign.cpn_cd) {
                                cli.info("cpn_cd not equal. set it to config " + campaign.cpn_cd)
                                config.cid = campaign.cpn_cd;
                                cfg.set('config', config);
                                cfg.save();
                            }
                            launchCampaign(campaign, 'launch');
                            break;

                        case 'player:load-content' :
                            var campaign = data.campaign;
                            cli.ok("WD - downloading content of campaign : " + campaign.cpn_name);
                            loadContent(campaign);
                            break;

                        case 'player:restart' :
                            cli.info('WD - initiating system restart !');
                            exec('shutdown -r -f', function (err) {
                                if (err) throw  err
                            });
                            break;

                        case 'player:shutdown' :
                            cli.info('WD - initiating system shutdown !');
                            exec('shutdown -s -f', function (err) {
                                if (err) throw  err
                            });
                            break;

                        case 'player:upgrade' :
                            cli.info('WD - initiating player upgrade!');
                            upgradePlayer(data.url);
                            break;

                    }
                } else {
                    console.log("WD - player id does not match");
                }
            })
        }
    });

    wd.serverSocketCluster.on('player:snapshot', function (data, res) {
        cli.info('SNAPSHOT REQUEST');
        exec('capture.exe', {cwd: __dirname}, function (error, stdout, stderr) {
            if (!error) {
                fse.readFile('MyImage.jpg', {encoding: 'base64'}, function (err, data) {
                    res(null, {img: data});
                })
            } else {
                console.log("received error")
                res(error);
            }
        });
    });

    wd.serverSocketCluster.on('player:delete:content', function (data, res) {
        var mediaFolder = path.join(__dirname,"/data/medias/");
        var uniqueMedias = {};
        var removed_files = [];
        jsonfile.readFile('campaign-wd.json', function (err, obj) {
        if (err){
            cli.error("campaign-wd read error: ",err);

            removeMedias(mediaFolder, [], res);

        }
            
        else{
            // launchCampaign(obj, 'check');
            uniqueMedias = obj.unique_medias;
            removeMedias(mediaFolder, uniqueMedias, res);

        }   
        });
    })

    function removeMedias(path, campaignFiles, res) {
        var removedFiles = [];
        fse.readdir(path, function(err, files) {
           if (err) {
                res(null, {success: false, data: err});

               console.log(err.toString());
           }
           else {

                files.map(function(file){
                    if(!InCampaign(file, campaignFiles)) {
                        removedFiles.push(path + file)
                    }
                });

                deleteFiles(removedFiles).then(function(deleted_medias){
                        res(null, {success: true, data: deleted_medias});
                    }).catch(function(err){
                        console.log("end of loop error :", err )
                        res(err);
                    })
                }
           });
        }

    function deleteFiles (files) {
        // body...
        var deletes = [];
        files.map(function(filepath){
            deletes.push(deleteFile(filepath))
        })

        return new Promise(function(resolve, reject){
            Promise.all(deletes)
            .then(function(response){
                resolve(response);
            }).catch(function(err){
                console.log("all promisses are error ", JSON.stringify(err))
                reject(err)
            })
        })
    }


    function deleteFile (filepath) {
        return new Promise(function(resolve, reject){
              fs.stat(filepath, function(err, stats) {
                if(err)
                    console.log("stat error ", err)

                if (stats.isFile()) {
                    fs.unlink(filepath, function(err) {
                        if (err) {
                            console.log(err.toString());
                            reject(err);
                        }
                        resolve(filepath)
                    });            
                }   
            });

        })
    }


                    

      function InCampaign(file, campaignFiles){
            var match = false;
            campaignFiles.map(function(obj){
                var fileArr =  obj.med_name.split('.');
                var fileName = obj.med_unique_name + "." + fileArr[1];
                if(fileName == file){
                    match = true;
                }

            })

            return match;
        }
    
    

    wd.serverSocketCluster.on('player:live', function (data, res) {
        cli.info('PLAYER LIVE');

        if (data == config.pid) { // Request Live Status
            res(null, {
                pid: config.pid,
                live: true
            });
        }

    });

     wd.serverSocketCluster.on('player:screenstatus', function (data, res) {
        cli.ok("requesting screen status");
        var done = null;
        screenstatus_promise = new Promise(function(resolve, reject){
            done = resolve;
        });
        screenstatus_promise.done = done;
 try
                                 {
                                     if(config.connect_screen)
                                         connectToScreen('status');
                                     else {
                                         wd.serverSocketCluster.emit('screenstatus:result', {
                                             pid: config.pid,
                                             screenstatus: {
                                                 result: 'error',
                                                 message: 'Not Supported'
                                             }
                                         })
                                         console.log("Set Connect Screen Config to true");
                                     }
                                 }
                                 catch(err)
                                 {
                                     wd.serverSocketCluster.emit('screenstatus:result', {
                                         pid: config.pid,
                                         screenstatus: {
                                             result: 'error',
                                             message: err
                                         }
                                     })
                                     console.log("Error fetching screen status; ", err);
                                 }
                             

      screenstatus_promise.then(function(screen_status){
            console.log("got screen status ", screen_status);
            // var data = JSON.parse(screen_status);

            res(null, screen_status);
        });
        // getPlayerStatus(res)
    });

    wd.serverSocketCluster.on('player:status', function (data, res) {
        console.info("requesting player status");
        var done = null;
        status_promise = new Promise(function(resolve, reject){
            done = resolve;
        });
        status_promise.done = done;
        wd.playerSocket.emit('player:status', {});

        status_promise.then(function(pc_status){
            var data = JSON.parse(pc_status);

            res(null, JSON.parse(data.param));
        })
        // getPlayerStatus(res)
    })

    wd.serverSocketCluster.on('disconnect', function (err) {
        cli.error("socket disconnected " + err);
        cli.info("un-subscribing on disconnect " + wd.serverSocketCluster.subscriptions(true));
        wd.serverSocketCluster.destroyChannel(config.client_key)
        cli.info("un-subscribed on disconnect " + wd.serverSocketCluster.subscriptions(true));

    });

    wd.serverSocketCluster.on('error', function (err) {
        // console.error("socket error : ", err.message);
    });

    wd.serverSocketCluster.on('subscribe', function () {
        cli.info("socket subscribed")
    });
    wd.serverSocketCluster.on('unsubscribe', function (socket, channel) {
        cli.info("socket unsubscribed")
    });
}

function getPlayerStatus(res) {
    try {
        exec("GetHardwareInfo.exe", {cwd: __dirname}, function (error, stdout, stderr) {

            if (!error) {
                var lines = stdout.split('\r\n');
                var jsonString = '{"';
                for (var i = 0; i < lines.length - 3; i++) {
                    var keyValue = lines[i].split('::');
                    var key = keyValue[0].trim();
                    var value = keyValue[1].trim();

                    jsonString += key + '":"' + value + '","';
                }

                keyValue = lines[i].split('::');
                key = keyValue[0].trim();
                value = keyValue[1].trim();
                jsonString += key + '":"' + value + '"}';
                var pcJson = JSON.parse(jsonString);
                console.log("PC status being sent to server: ", JSON.stringify(pcJson));

                res(null, {success: true, status: pcJson})
            }
        });
    }
    catch (err) {
        console.error("Error fetching pc status; ", err);
        res(null, {success: false, msg: err})
    }
}

function registerNode() {
    var playerAddress = getPlayerAddress(os.networkInterfaces());
    cli.info("Registering WD. [NAME] " + config.player_name + " [IPv4] : " + playerAddress.ip + " [MAC] : " + playerAddress.mac);

    wd.serverSocketCluster.emit('player:register', {        // send register request with: player_name.client_key,mac_address
        player_name: config.player_name,
        mac_address: playerAddress.mac,
        ip_address: playerAddress.ip,
        client_key: config.client_key
    }, function (err, res) {
        if (err) {
            cli.error("Register WD Error : " + err);
        }
        if (res.valid === true) {
            cli.ok('Updating player id to ' + res.pid)
            config.pid = res.pid;
            cfg.set('config', config);
            cfg.save();
            connectNode();
        } else if (res.valid === false) {
            //shutOff(res, wd);
        }
        else {
        }
    })
}

function connectNode() {
    var playerAddress = getPlayerAddress(os.networkInterfaces());
    cli.info("Connecting WD. [IPv4] : " + playerAddress.ip + " [MAC] : " + playerAddress.mac);

    wd.serverSocketCluster.emit('player:connect', {
        pid: config.pid,
        client_key: config.client_key,
        mac_address: playerAddress.mac
    }, function (err, res) {
        if (err) {
            cli.error("Connect WD Error : " + err);
        }
        else if (res.valid === true) {

            cli.info("WD Connected");
            server_reconnect_count = 0;
            requestCampaign();
        }
        else if (res.valid === false) {
            cli.error('WD Connect INVALID: ' + res.valid);

            if (!config.server_reconnect_on_validation_failure) {
                //exit watchdog, leave player on
                //shutOff(res, wd);
            }
            else {
                if (server_reconnect_count < config.server_reconnect_limit) {
                    cli.error("Validation failed! Reconnecting in 20s ...");
                    server_reconnect_count++;
                    //email("Server validation for player failed. Reconnect attempt: " + server_reconnect_count);

                    setTimeout(function () {
                        connectNode();
                    }, 20000);
                }
                else {
                    cli.error("Reconnect attemp limit reached. Shutting down watchdog in 10s");
                    //email("Server validation for player failed. Reconnect attempt limit reached: " + server_reconnect_count);
                    setTimeout(function () {
                        process.exit();
                    }, 10000);
                }
            }
        }
        else {
            cli.error('WD Connect NONE: ' + res.valid);
        }
    });
}

function requestCampaign() {
    wd.serverSocketCluster.emit('campaign:check', {
        plr_cd: config.pid,
        client_key: config.client_key,
        cpn_cd: config.cid,
        cpn_date: config.cpn_date
    }, function (err, res) {
        if (err) {
            //runOfflineCampaign();
            cli.info("Request Campaign error " + err);
            // log.write("Request Campaign error " + err);
        }
        else {

            if (res.valid === true && res.update === true) {       // Connect valid
                var campaign = res.campaign;
                console.info("config cpn_cd  :" + config.cid);
                console.info("campaign cd    :" + campaign.cpn_cd);
                console.info("config cpn_date    :" + config.cpn_date);
                console.info("campaign updatedAt :" + campaign.updatedAt);
                config.cpn_date = campaign.updatedAt;
                config.cid = campaign.cpn_cd;
                cfg.set('config', config);
                cfg.save();
                // if (config.cid !== campaign.cpn_cd) {
                //     cli.ok('new campaign received : ' + res.campaign.cpn_name);
                // } else if (config.cpn_date !== campaign.updatedAt) {
                //     cli.ok('campaign updated : ' + res.campaign.cpn_name);
                // } else {
                //     cli.ok('no change in campaign : ' + res.campaign.cpn_name);
                // }
                cli.info("0 - " + res.msg);
                launchCampaign(campaign, 'launch');
            } else if (res.valid === true && res.update == false) {   // Connect INVALID
                //check for json file saved. if exists run launchcampaign using the content.

                cli.info("1 - " + res.msg);
                checkWdCampaign();
            } else if (res.valid === false) {   // Connect INVALID
                cli.info("2 - " + res.msg);
            } else {
                //runOfflineCampaign();
                cli.error('unknown error...');
            }
        }
    })
}

function checkWdCampaign (){
    jsonfile.readFile('campaign-wd.json', function (err, obj) {
        if (err)
            cli.error("campaign-wd read error: " + err);
        else{
            cli.info("Checking media content.");
            launchCampaign(obj, 'check');
        }
    })
}

function launchCampaign(campaign,origin) {
    // var check_flag = (typeof check == 'undefined') ? false : check;
   
    if (origin == 'launch') {
        saveCampaignJson('campaign-wd.json', campaign);
    }
    cli.info('Downloading campaign content: ' + campaign.unique_medias.length);
    downloadFiles(campaign.unique_medias)
        .then(function (download_ing) {

        if(!download_ing){
            eventEmitter.emit('download-completed');

            // emit download completed here. 

            cli.info("Download completed " + download_ing);    
            config.downloadedAt = new Date();
            cfg.set('config', config);
            cfg.save();

            if (wd.playerSocket !== null) {
                cli.info("Emitting to player with campaign");
                wd.playerSocket.emit('campaign_updated', {
                    campaign: campaign
                });}

            if((new_downloads && (origin == 'check')) || origin == 'launch') {
                wd.serverSocketCluster.emit('campaign:downloaded', {
                    plr_cd: config.pid,
                    client_key: config.client_key,
                    downloadedAt: config.downloadedAt
                }, function (err) {
                    if (err) {
                        cli.error(err);
                    } else {
                        new_downloads = false;
                        cli.ok("Download timestamp updated at check.");
                    }
                })}
        }

    }).catch(function (err) {
        cli.info("Relaunch in 3 seconds... ");
        setTimeout(function(){ 
            launchCampaign(campaign, origin);
         }, 1500);

        
        saveCampaignJson('campaign-wd.json', campaign);
    })
}

function loadContent(campaign) {
    // var check_flag = (typeof check == 'undefined') ? false : check;
    if(downloading){
        cli.info("Queue loading new content.");
        eventEmitter.on('download-completed', function(){
        cli.error("this function hsouldnt be executed yet");
             loadContent(campaign);
        });
    }else{

        cli.info('Downloading campaign content: ' + campaign.unique_medias.length);
        downloadFiles(campaign.unique_medias)
            .then(function (download_ing) {

                if(!download_ing){
                    cli.info("Download of content for " + campaign.cpn_name + ' completed.');
                    // start listening to on complete from main camplain load sequence launchCampaign, when completed start loading
                }

            }).catch(function (err) {
            cli.info("Retry download in 15s seconds... ");
            setTimeout(function(){
                loadContent(campaign);
            }, 15000);
        })
    }
}

function saveCampaignJson(fileName, obj){
    jsonfile.writeFile(fileName, obj, function (err) {
        if (err)
            cli.error("campaign-wd save error: ",err);
        else{
            cli.info("Campaign file saved")
        }
    });
}

function downloadFiles(unique_medias) {
    var downloads = [];


        // var mediaRootPath = __dirname + config.media.media_path;
        var mediaRootPath = path.join(__dirname,config.media.media_path);

        return new Promise(function (fulfill, reject) {      
            if(downloading){
                return fulfill(true);
            } 
            downloading = true
            // fse.ensureDir(mediaRootPath).then(function () { 
            for (var i = 0; i < unique_medias.length; i++) {
                downloads.push(checkForDownload(i, unique_medias[i]));
            }

            Promise.all(downloads).then(function (completed_downloads) {
                    // cli.ok("Downloads finished : "+ JSON.stringify(completed_downloads));
                    var unfinished_downloads = completed_downloads.filter(function(download){
                        return download != null;
                    });
                    if (unfinished_downloads.length > 0){
                        downloading = false;
                        return reject(unfinished_downloads);
                    }else {
                        downloading = false;
                        return fulfill(false)
                    }

                },
                function (err) {
                    return reject(err)
                });
        
        });
}
            // }).catch(function (err) {
            //     cli.error('ensureDir ' + mediaRootPath + ' error :' + err)
            //     return reject(err);
            // })

function checkForDownload(index, unique_media) {
    var mediaRootPath = path.join(__dirname,config.media.media_path);
    var url = "http://" + config.cdn_server_ip + ":" + config.cdn_server_port + "/watchdog" + unique_media.med_url;
    var fileSum = unique_media.med_hash_checksum;
    var filename = url.substring(url.lastIndexOf("/") + 1).split("?")[0];
    var ext = unique_media.med_path.substring(unique_media.med_path.lastIndexOf("."));
    var filepath = mediaRootPath + filename + ext;
    unique_media.path = filepath;
    // return new Promise(function (fulfill, reject) { 
    return new Promise(function (fulfill, reject) { 
        if(unique_media.med_mimetype.indexOf('application') !== -1){
            checksum.file(filepath, function (err, sum) {
                if (sum == undefined || sum != fileSum) {
                   reqDownload(index,url, filepath, 10000, fulfill, reject, unique_media); 
                } else {
                    fulfill();
                }
            });
        } else {
            fs.stat(filepath, function(err, stats){
                if(err)
                    reqDownload(index, url, filepath, 10000, fulfill, reject, unique_media);
                else{
                    cli.ok('['+index + "] File Available :" + unique_media.med_cd + ' Name: ' + unique_media.med_name);
                    fulfill();
                }
            })
        }   
    });

};

function reqDownload(index, url, filepath, temeout, fulfill, reject, unique_media){

    new_downloads = true;
    var options = {
        url: url,
        timeout: temeout
    };
    req = request(options);
    req.on('response', function (res) {
        cli.ok('['+index + "] Receiving file: " + unique_media.med_cd + ' Name: ' + unique_media.med_name);
    });
    req.on('data', function (chunk) {
    }).pipe(fse.createWriteStream(filepath + '.temp'))
        .on('error', function (err) {
            cli.error("ERROR DOWNLOAD: " + err);

            fulfill(unique_media)
        }).on('finish', function () {
        }).on('close', function (err) {
            if(err) {
                cli.error("Download closed : ", err);
                fulfill(unique_media);
            }
            cli.ok('['+index + "] Download completed :" + unique_media.med_cd + ' Name: ' + unique_media.med_name);
            unique_media.path = filepath;

            fs.createReadStream(filepath + '.temp').pipe(fs.createWriteStream(filepath))
            .on('error', function(err){
                cli.error(err)
                reject(err)
            })
            .on('close', function(arg){
                fulfill();
                 fs.unlink(filepath + '.temp', function(err) {
                    if (err) {
                        cli.error('Remove temp file ' + err.toString());
                    }
                });              
            })
        });
    req.on('error', function (err) {
        cli.error('['+index + "] Error downloading media: " + unique_media.med_cd + ' Name: ' + unique_media.med_name + " " + err);
        fulfill(unique_media);
    });
}

function getPlayerAddress(interfaces) {

    var addresses = [];
    for (var k in interfaces) {
        for (var k2 in interfaces[k]) {
            var address = interfaces[k][k2];
            if (address.family === 'IPv4' && !address.internal) {
                addresses.ip = address.address;
                addresses.mac = address.mac;
            }
        }
    }
    return addresses;
}

function upgradePlayer(url) {
    cli.info(url);
    var filesize = 20000000;
    process.update_ready = false;
    var req = request({url: url});
    var bar;

    req.on('data', function (chunk) {

    })
        .pipe(fse.createWriteStream('installer.exe'))
        .on('close', function (err) {
            cli.ok('finish downloading');
            console.log("installer.exe /SP- /verysilent key=" + config.client_key + " /noicons")

            exec('"installer.exe" /SP- /verysilent key="' + config.client_key + '" /noicons',
                function (error, stdout, stderr) {
                    process.update_ready = false;

                    console.log('stdout: ' + stdout);
                    console.log('stderr: ' + stderr);
                    if (error) {
                        console.log('exec error: ' + error);
                        process.update_ready = true;
                    }
                });
        })
        .on('error', function (err) {
            cli.error(err);
        });
}

function connectToPlayer() {
    // cli.info('Listening to Player ...')

    wd.playerSocketListener = require('socket.io').listen(2727, {log: false});
    wd.playerSocketListener.set('heartbeat timeout', 20000);
    wd.playerSocketListener.set('heartbeat interval', 20000);
    // wd.playerSocketListener.set("log level", 0);
    wd.playerSocketListener.on('connection', function (socket) { 
        cli.ok('Player Connected');
        player_heartbeat_time = (new Date()).getTime();
        // log.write('Player Connected');

        wd.playerSocket = socket;

        // cli.info('Sending startup arguments to player');
        // log.write('Sending startup arguments to player');

        wd.playerSocket.emit('startup', {
            rootpath: __dirname + '/data/player',
            pid: config.pid
        });

        //check player status every x minutes
        var currentTime;
        var playerHeartbeatTimer = setInterval(function () {
            currentTime = (new Date()).getTime();
            var timeDiff = currentTime - player_heartbeat_time;
            console.log("Time Difference: ", timeDiff);

            if (timeDiff > 2 * 60000 * config.player_heartbeat_interval) {
                console.log("Player is not sending heartbeat for the interval of milliseconds: ", timeDiff);
                email("Potential player shutdown. Not sending heartbeat for the interval of milliseconds: " + timeDiff);
                console.log("PC restarting in 5s ...");
                setTimeout(function () {
                    exec('shutdown -r -f', function (err) {
                        if (err) throw  err
                    });
                }, 5000);
            }
            wd.playerSocket.emit('is_live', {});
        }, 60000 * config.player_heartbeat_interval);

        // if (wd.serverSocketCluster !== null)
        //     requestCampaign();

        wd.playerSocket.on('live', function (status) {
            //console.log("Received heartbeat from player: ", status);
            player_heartbeat_time = (new Date()).getTime();

            //PC Alert
            // if (status == 'hdd_alert') {
            //     email("HDD Alert: Harddisk Usage more than 95%.");
            // }
            // else if (pc_alert_count < max_pc_alert_count) {
            //     if (status == 'mem_alert') {
            //         if (pc_alert_count % 5 == 0) {
            //             email("Memory Alert: Memory Usage more than 95%. Alert Count: " + pc_alert_count);
            //         }
            //         pc_alert_count++;
            //     }
            //     else if (status == 'cpu_alert') {
            //         if (pc_alert_count % 5 == 0) {
            //             email("CPU Alert: CPU Usage more than 95%. Alert Count: " + pc_alert_count);
            //         }
            //         pc_alert_count++;
            //     }
            // }
        });

        // wd.playerSocket.on('pc_status', function (pc_status) {
        //     console.log("PC status received from player: ", pc_status);
        //     if (pc_status !== undefined && pc_status !== null) {
        //         wd.serverSocketCluster.emit('pcstatus:result', {
        //             pid: config.pid,
        //             pcstatus: pc_status
        //         })
        //     }
        // });

        wd.playerSocket.on('player:status', function (pc_status) {
            status_promise.done(pc_status);           
        });

        wd.playerSocket.on('playerlogs', function (playerlogs) {
            cli.info('Received logs from player');
            //log.write('Received logs from player');

            //var watchdoglogs = fse.readFileSync('logs.json', 'utf8');
            //log.flush();

            if (config.debugging) {
                var logs = 'Logs reported at ' + new Date().toLocaleString() + '\n' +
                    '{"Player Logs" : ' + playerlogs +
                    '}'; //,"Watchdog logs" : [' + watchdoglogs + ']}';
                email(logs);
            }
        });

        wd.playerSocket.on('apperr', function (err) {
            cli.info('Received error logs from player', err);
            email('Error Logs: ' + err);
        });

        wd.playerSocket.on('appcrash', function (logs) {
            cli.info('Player crashed ... \n')
            //log.write('shutting down all services ...');
            wd.playerSocket.disconnect();
            if (config.restart_player_on_failure) {
                closePlayer().then(function () {
                    if (restart_count < config.restart_limit) {
                        console.log("Restarting player in 5s ...")
                        setTimeout(function () {
                            runPlayer();
                            restart_count++;
                            console.log("Player restarted");
                            email('Player[' + config.pid + '] crashed and restarted for ' + restart_count + ' times. Please check the logs ' + logs);
                        }, 5000);
                    }
                    else
                        email('Player[' + config.pid + '] crashed and has reached restart limit of ' + restart_count + ' times. Please check the logs ' + logs);
                });
            }
            else
                email('Player[' + config.pid + '] crashed. Please check the logs ' + logs);
        });

        wd.playerSocket.on('disconnect', function () {
            cli.info("Player Socket Disconnected");
            clearInterval(playerHeartbeatTimer);
        });
    });
}

function connectToScreen(screen_command, res) {
    wd.screenSocket = new net.Socket();
    wd.screenSocket.connect(config.screen_port, config.screen_ip, function () {
        cli.ok('CONNECTED TO Screen at: ' + config.screen_ip + ':' + config.screen_port);

        if(screen_command !== "")
        {
            var chunk = 'AA00010001';
            switch (screen_command){
                case 'status'    : chunk = 'AA00010001'; break;
                case 'mute_on'   : chunk = 'AA1301010116'; break;
                case 'mute_off'  : chunk = 'AA1301010015'; break;
                case 'power_on'  : chunk = 'AA1101010114'; break;
                case 'power_off'  : chunk = 'AA1101010013'; break;
            }
            wd.screenSocket.write(new Buffer(chunk, 'hex'));
            console.log("Message Sent to TV");

            if(screen_command != 'status')
                wd.screenSocket.destroy();
        }
    });

    wd.screenSocket.on('error',function (error) {
        console.log("error:" + error);

                    res(null, {
            pid: config.pid,
            screenstatus: {
                result: 'error',
                message: error
            }
        });


        wd.screenSocket.destroy();
    })

    wd.screenSocket.on('data', function(d) {
        var data  =d.toString('hex').toUpperCase();
        console.log("screen data: ", data);
        var screenJson = {};

        switch (screen_command) {

            case 'status' : {
                var input = ''
                switch (data.slice(18, data.length-8)) {
                    case '14' : input = 'PC'
                    case '21' : input = 'HDMI'
                    case '22' : input = 'HDMI_PC'
                }
                var aspect = ''
                switch (data.slice(20, data.length-6)) {
                    case '10' : aspect = '16:9'
                    case '18' : aspect = '4:3'
                    case '20' : aspect = 'original ratio'
                }

                var screenJson = {
                    result: 'success',
                    data: {
                        power: data.slice(12, data.length - 14),
                        volume: data.slice(14, data.length - 12),
                        mute: data.slice(16, data.length - 10),
                        input: input,
                        aspect: aspect
                    }
                }


                    res(null, screenJson);

                console.log("Message Received from TV:", screenJson);
                break;
            }

            //case 'mute_on' :{
            //    //console.log(data.slice(1, data.length-1))
            //    result.result.push({ mute : data.slice(12, data.length-2)})
            //    res.send(result)
            //}
            //
            //case 'mute_off' :{
            //
            //    result.result.push({ mute : data.slice(12, data.length-2)})
            //    res.send(result)
            //}
        }

        wd.screenSocket.destroy();
    });
// Add a 'close' event handler for the client socket
    wd.screenSocket.on('close', function() {
        console.log('Connection to screen closed');
    });
}

function email(data) {
    wd.serverSocketCluster.emit('player:email', {
        subject: data.subject,
        content: data.content,
        pid: config.pid
    })
}